<style>
   .sidebar-header img {
       width: 100px!important;
   }
</style>
<div class="sidebar-wrapper" data-simplebar="true">
   <div class="sidebar-header">
      <div>
         <img src="{{ asset('frontend/assets/img/logo/logop.png') }}" class="logo-icon" alt="logo">
      </div>
      <div>
         <h4 class="logo-text">{{ Auth::user()->name }}</h4>
      </div>
      <div class="toggle-icon ms-auto"><i class='bx bx-arrow-to-left'></i>
      </div>
   </div>
   <!--navigation-->
   <ul class="metismenu" id="menu">
      <li>
         <a href="{{ route('admin.dashboard') }}">
            <div class="parent-icon"><i class='bx bx-home-circle'></i>
            </div>
            <div class="menu-title">Dashboard (NEST)</div>
         </a>
      
      </li>

      <li>
         <a href="javascript:;" class="has-arrow">
            <div class="parent-icon"><i class="bx bx-category"></i>
            </div>
            <div class="menu-title">Manage HeroArea</div>
         </a>
         <ul>
            <li> <a href="{{ route('view_home') }}"><i class="bx bx-right-arrow-alt"></i>Home</a>
            </li>
         </ul>
      </li>

      
      <li>
         <a href="javascript:;" class="has-arrow">
            <div class="parent-icon"><i class="bx bx-category"></i>
            </div>
            <div class="menu-title">Manage AboutArea</div>
         </a>
         <ul>
            <li> <a href="{{ route('view_about') }}"><i class="bx bx-right-arrow-alt"></i>About</a>
            </li>
         </ul>
         <ul>
            <li> <a href="{{ route('multiImg') }}"><i class="bx bx-right-arrow-alt"></i>Add MultiImg</a></li>
            <li> <a href="{{ route('all_img') }}"><i class="bx bx-right-arrow-alt"></i>All MultiImg</a></li>
         </ul>
      </li>

      <li>
         <a href="javascript:;" class="has-arrow">
            <div class="parent-icon"><i class="bx bx-category"></i>
            </div>
            <div class="menu-title">Manage Services</div>
         </a>
         <ul>
            <li> <a href="{{ route('view_service_list') }}"><i class="bx bx-right-arrow-alt"></i>Service List</a></li>
            <li> <a href="{{ route('view_service') }}"><i class="bx bx-right-arrow-alt"></i>Service</a></li>
         </ul>
      </li>

      <li>
         <a href="javascript:;" class="has-arrow">
            <div class="parent-icon"><i class="bx bx-category"></i>
            </div>
            <div class="menu-title">Manage PortfolioArea</div>
         </a>
         <ul>
            <li> <a href="{{ route('view_portfolio') }}"><i class="bx bx-right-arrow-alt"></i>View Portfolio</a>
            </li>
         </ul>
     
      </li>

      <li>
         <a href="javascript:;" class="has-arrow">
            <div class="parent-icon"><i class="bx bx-category"></i>
            </div>
            <div class="menu-title">Blog Category</div>
         </a>
         <ul>
            <li> <a href="{{ route('view_blog_category') }}"><i class="bx bx-right-arrow-alt"></i>View Blog Category</a></li>
            <li> <a href="{{ route('index') }}"><i class="bx bx-right-arrow-alt"></i>View All Blog</a></li>
            <li> <a href="{{ route('add_blog') }}"><i class="bx bx-right-arrow-alt"></i>Add Blog</a></li>
         </ul>
     
      </li>


      <li>
         <a href="javascript:;" class="has-arrow">
            <div class="parent-icon"><i class="bx bx-category"></i>
            </div>
            <div class="menu-title">Vendor Manage</div>
         </a>
         <ul>
            <li> <a href="{{ route('pending_vendor') }}"><i class="bx bx-right-arrow-alt"></i>Pending Vendor</a>
            </li>
         </ul>
         <ul>
            <li> <a href="{{ route('approve_vendor') }}"><i class="bx bx-right-arrow-alt"></i>Approve  Vendor</a>
            </li>
         </ul>
      </li>

      <li>
         <a href="javascript:;" class="has-arrow">
            <div class="parent-icon"><i class="bx bx-category"></i>
            </div>
            <div class="menu-title">Footer Manage</div>
         </a>
         <ul>
            <li> <a href="{{ route('view_footer') }}"><i class="bx bx-right-arrow-alt"></i>View Footer</a>
            </li>
         </ul>
      </li>

      <li>
         <a href="javascript:;" class="has-arrow">
            <div class="parent-icon"><i class="bx bx-category"></i>
            </div>
            <div class="menu-title">Contact Manage</div>
         </a>
         <ul>
            <li> <a href="{{ route('view_contact') }}"><i class="bx bx-right-arrow-alt"></i>View Contact</a>
            </li>
         </ul>
      </li>


      <li>
         <a href="https://themeforest.net/user/codervent" target="_blank">
            <div class="parent-icon"><i class="bx bx-support"></i>
            </div>
            <div class="menu-title">Support</div>
         </a>
      </li>
   </ul>
   <!--end navigation-->
</div>