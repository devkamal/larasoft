@extends('frontend.master')
@section('content')
<main class="main pages">
      <div class="page-header breadcrumb-wrap">
      <div class="container">
            <div class="breadcrumb">
                  <a href="index.html" rel="nofollow"><i class="fi-rs-home mr-5"></i>Home</a>
                  <span></span> Pages <span></span> My Account
            </div>
      </div>
      </div>
      <div class="page-content pt-150 pb-150">
      <div class="container">
            <div class="row">
                  <div class="col-xl-8 col-lg-10 col-md-12 m-auto">
                  <div class="row">
                        <div class="col-lg-12 col-md-12">
                              <div class="login_wrap widget-taber-content background-white">
                              <div class="padding_eight_all bg-white">
                                    <div class="heading_s1">
                                          <h1 class="mb-5">Create an Vendor Account</h1>
                                          <p class="mb-30">Already have an account? <a href="{{ route('vendor_login') }}">Login</a></p>
                                    </div>
                                    <form method="POST" action="{{ route('vendor_register_store') }}" id="myForm">
                                          @csrf

                                          <div class="form-group">
                                          <input type="text" name="name" placeholder="Shop Name" id="name">
                                          @error('name')
                                                <span style="color:red">{{ $message }}</span>
                                          @enderror
                                          </div>

                                          <div class="form-group">
                                          <input type="text" name="username" placeholder="User Name">
                                          @error('username')
                                                <span style="color:red">{{ $message }}</span>
                                          @enderror
                                          </div>

                                          <div class="form-group">
                                          <input type="email" name="email" id="email" placeholder="Email">
                                          @error('email')
                                                <span style="color:red">{{ $message }}</span>
                                          @enderror
                                          </div>

                                          <div class="form-group">
                                          <input type="number" name="phone" placeholder="Phone Number" id="name">
                                          @error('phone')
                                                <span style="color:red">{{ $message }}</span>
                                          @enderror
                                          </div>

                                          <div class="form-group">
                                          <select name="vendor_join" id="vendor_join" class="form-control">
                                                <option value="">Select join year</option>
                                                <option value="2023">2023</option>
                                                <option value="2024">2024</option>
                                                <option value="2025">2025</option>
                                                <option value="2026">2026</option>
                                          </select>
                                          @error('vendor_join')
                                                <span style="color:red">{{ $message }}</span>
                                          @enderror
                                          </div>

                                          <div class="form-group">
                                          <input type="password" id="password" name="password" placeholder="Password">
                                          @error('password')
                                                <span style="color:red">{{ $message }}</span>
                                          @enderror
                                          </div>
                                          <div class="form-group">
                                          <input type="password" id="password_confirmation" name="password_confirmation" placeholder="Confirm password">
                                          @error('password_confirmation')
                                                <span style="color:red">{{ $message }}</span>
                                          @enderror
                                          </div>
                                    
                                    
                                          <div class="login_footer form-group mb-50">
                                          <div class="chek-form">
                                                <div class="custome-checkbox">
                                                      <input class="form-check-input" type="checkbox" name="checkbox" id="exampleCheckbox12" value="">
                                                      <label class="form-check-label" for="exampleCheckbox12"><span>I agree to terms &amp; Policy.</span></label>
                                                </div>
                                          </div>
                                          <a href="page-privacy-policy.html"><i class="fi-rs-book-alt mr-5 text-muted"></i>Lean more</a>
                                          </div>
                                          <div class="form-group mb-30">
                                          <button type="submit" class="btn btn-fill-out btn-block hover-up font-weight-bold">Submit &amp; Register</button>
                                          </div>
                                          <p class="font-xs text-muted"><strong>Note:</strong>Your personal data will be used to support your experience throughout this website, to manage access to your account, and for other purposes described in our privacy policy</p>
                                    </form>
                              </div>
                              </div>
                        </div>
                        <div class="col-lg-6 pr-30 d-none d-lg-block">
                              <div class="card-login mt-115">
                              <a href="#" class="social-login facebook-login">
                                    <img src="assets/imgs/theme/icons/logo-facebook.svg" alt="">
                                    <span>Continue with Facebook</span>
                              </a>
                              <a href="#" class="social-login google-login">
                                    <img src="assets/imgs/theme/icons/logo-google.svg" alt="">
                                    <span>Continue with Google</span>
                              </a>
                              <a href="#" class="social-login apple-login">
                                    <img src="assets/imgs/theme/icons/logo-apple.svg" alt="">
                                    <span>Continue with Apple</span>
                              </a>
                              </div>
                        </div>
                  </div>
                  </div>
            </div>
      </div>
      </div>
</main>

<!--===Validation JS==------>
<script type="text/javascript">
$(document).ready(function (){
  $('#myForm').validate({
      rules: {
           name: {
              required : true,
          }, 
          username: {
              required : true,
          }, 
      },
      messages :{
            name: {
              required : 'Please Enter Shop Name',
          },
          username: {
              required : 'Please Enter user Name',
          },
      },
      errorElement : 'span', 
      errorPlacement: function (error,element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
      },
      highlight : function(element, errorClass, validClass){
          $(element).addClass('is-invalid');
      },
      unhighlight : function(element, errorClass, validClass){
          $(element).removeClass('is-invalid');
      },
  });
});

</script>

@endsection
