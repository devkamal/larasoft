@extends('admin.master')
@section('content')

<div class="page-wrapper">
      <div class="page-content"> 
<div class="container" style="margin-top: 50px;">
    <div class="row">
        <div class="col-md-12">
            <h3>
                <a href="{{ route('add_service') }}" class="btn btn-primary">Add Service</a>
            </h3>
        </div>
        <div class="col-md-12">      
            <table id="example" class="table table-striped" style="width:100%">
                <thead>
                    <tr>
                        <th> Image </th>
                        <th> Title </th>
                        <th> Tags </th>
                        <th> description</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($allData as $key => $item)
                        <tr>
                            <td>
                              <img src="{{ asset($item->image) }}" alt="img" width="60px">
                            </td>
                            <td>{!! $item->title !!}</td>
                            <td>{{ $item->tags }}</td>
                            <td>{{ $item->description }}</td>
                            <td>
                                <a title="Edit" class="btn btn-primary" href="{{ route('edit_service',$item->id) }}">Edit</a>
                                <a title="Delete" class="btn btn-danger" href="{{ route('delete_service',$item->id) }}"
                                    id="delete">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
</div>
        

@endsection
