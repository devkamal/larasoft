@extends('admin.master')
@section('content')

<div class="page-wrapper">
      <div class="page-content"> 
<div class="container" style="margin-top: 50px;">
    <div class="row">
        <div class="col-md-12">
            <h3>
                <a class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#addlist">Add ServiceList</a>
            </h3>
        </div>
        <div class="col-md-12">      
            <table id="example" class="table table-striped" style="width:100%">
                <thead>
                    <tr>
                        <th>Sl:</th>
                        <th> Service Name </th>
                        <th> Service List </th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($allData as $key => $item)
                        <tr>
                              <td>{{ $key+1}}</td>
                            <td>{!! $item['service']['title'] !!}</td>
                            <td>{{ $item->name }}</td>
                            <td>
                                <a title="Edit" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#editServicelist{{$item->id}}">Edit</a>
                                <a title="Delete" class="btn btn-danger" href="{{ route('delete_service_list',$item->id) }}"
                                    id="delete">Delete</a>
                            </td>
                        </tr>

                        <!-- Edit Coupon Modal -->
                        <div class="modal fade" id="editServicelist{{$item->id}}" tabindex="-1" style="display: none;" aria-hidden="true">
                          <div class="modal-dialog modal-lg modal-dialog-centered">
                                <div class="modal-content">
                                      <div class="modal-header">
                                            <h5 class="modal-title text-black">Edit Service List</h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                      </div>
                                      <form action="{{route('update_service_list',$item->id)}}" method="post">
                                            @csrf

                                            <div class="modal-body text-black">

                                              <div class="col-12">
                                                <label for="category">Service Name</label>
                                                <select name="service_id" id="service_id" class="form-control">
                                                  <option value="">Select Service</option>
                                                  @foreach ($services as $service)
                                                  <option value="{{ $service->id }}" {{ $service->id == $item->service_id? 'selected' : ''}}>{{ $service->title }}</option>
                                                  @endforeach
                                                </select>
                                              </div>

                                              <div class="col-md-12">
                                                    <label for="name">Service List Name</label>
                                                    <input type="text" name="name" value="{{$item->name}}" id="name" class="form-control">
                                              </div>


                                            </div>
                                            <div class="modal-footer">
                                                  <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                                                  <button type="submit" class="btn btn-dark" id="updateCoupon">Save changes</button>
                                            </div>
                                      </form>
                                </div>
                          </div>
                        </div>

                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
</div>



<!-- Modal -->
<div class="modal fade" id="addlist" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Service List</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <form action="{{ route('store_service_list') }}" method="post">
        @csrf 
      <div class="modal-body">
    

        <div class="row">

            <div class="col-md-12">
                <label for="service_id">Service Name</label>
                <select name="service_id" id="" class="form-control">
                  <option value="">Select Service Name</option>
                  @foreach ($services as $item)
                        <option value="{{$item->id}}">{{$item->title}}</option>
                  @endforeach
                </select>
                @error('service_id')
                    <span style="color:red">{{ $message }}</span>
                @enderror
            </div>

            <div class="col-md-12 mt-1">
                <label for="name">Service List Name</label>
                <input type="text" name="name" placeholder="Add your name" class="form-control">
                @error('name')
                    <span style="color:red">{{ $message }}</span>
                @enderror
            </div>


        </div>
  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>
        

@endsection
