@extends('admin.master')
@section('content')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

@if (session()->has('success'))
    <div class="alert alert-success">
        {{session()->get('success')}}
    </div>
@endif

<div class="page-wrapper">
      <div class="page-content"> 
<div class="container mt-4">
    <div class="row">
        <div class="col-md-12">
            <h3>
                <a href="{{ route('view_service') }}">Back</a>
            </h3>
        </div>
    </div>
    <form action="{{ route('update_service',$editData->id ) }}" method="post" enctype="multipart/form-data">
        @csrf 

        
        <div class="row">
            <div class="col-md-12">
                <label for="tags">Tags</label>
                <input type="text" name="tags" value="{{ $editData->tags }}" class="form-control">
                @error('tags')
                    <span style="color:red">{{ $message }}</span>
                @enderror
            </div>

            <div class="col-md-12">
                <label for="title">Service Title</label>
                <input type="text" name="title" value="{{ $editData->title }}" class="form-control">
                @error('title')
                    <span style="color:red">{{ $message }}</span>
                @enderror
            </div>

            <div class="col-md-12">
                <label for="dsc">Service Descriptions</label>
              
                <textarea name="description" aria-hidden="true" class="form-control">
                    {!! $editData->description !!}
                </textarea>
                @error('description')
                    <span style="color:red">{{ $message }}</span>
                @enderror
                
            </div>

            <div class="col-md-12">
                <label for="image">Service Image</label>
                <input type="file" name="image" id="image" class="form-control">
                @error('image')
                    <span style="color:red">{{ $message }}</span>
                @enderror
                <img id="showImg" src="{{ asset($editData->image), url('upload/noimg.png') }}" 
                alt="img" width="50px;">
                <input type="hidden" name="oldimg" value="{{ $editData->image }}">
            </div>
            <div class="col-md-12 mt-4">
                <button class="btn btn-primary form-control">Submit</button>
            </div>
        </div>
    </form>
</div>
</div>
</div>



  <script type="text/javascript">
        $(document).ready(function(){
            $('#image').change(function(e){
                var reader = new FileReader();
                reader.onload = function(e){
                    $('#showImg').attr('src',e.target.result);
                }
                reader.readAsDataURL(e.target.files['0']);
            });
        });
  </script>


@endsection
