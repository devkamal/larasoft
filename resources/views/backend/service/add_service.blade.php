@extends('admin.master')
@section('content')

<div class="page-wrapper">
      <div class="page-content"> 
<div class="container mt-4">
    <div class="row">
        <div class="col-md-12">
            <h3>
                <a href="{{ route('view_service') }}">Back</a>
            </h3>
        </div>
    </div>
    <form action="{{ route('store_service') }}" method="post" enctype="multipart/form-data" id="myForm">
        @csrf 

        <div class="row">
            <div class="col-md-12">
                <label for="phone">Image</label>
                <input type="file" name="image" placeholder="Add your image" class="form-control">
                @error('image')
                    <span style="color:red">{{ $message }}</span>
                @enderror
            </div>

            <div class="col-md-12">
                <label for="title">Title</label>
                <input type="text" name="title" placeholder="Add your title" class="form-control">
                @error('title')
                    <span style="color:red">{{ $message }}</span>
                @enderror
            </div>

            <div class="col-md-12">
                <label for="dsc">Descriptions</label>
              
                <textarea name="description" aria-hidden="true" class="form-control"></textarea>
                @error('description')
                    <span style="color:red">{{ $message }}</span>
                @enderror
                
            </div>

            <div class="col-md-12">
                <label for="tags">Tags</label>
                <input type="text" name="tags" placeholder="Add your tags" class="form-control">
                @error('tags')
                    <span style="color:red">{{ $message }}</span>
                @enderror
            </div>

            <div class="col-md-12 mt-4">
                <button class="btn btn-primary form-control">Submit</button>
            </div>
        </div>
    </form>
</div>
</div>
</div>


@endsection
