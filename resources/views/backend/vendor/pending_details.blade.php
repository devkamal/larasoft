@extends('admin.master')
@section('content')

<div class="page-wrapper">
      <div class="page-content"> 
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                  <div class="breadcrumb-title pe-3">Admin Profile</div>
                  <div class="ps-3">
                        <nav aria-label="breadcrumb">
                              <ol class="breadcrumb mb-0 p-0">
                                    <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Admin Profilep</li>
                              </ol>
                        </nav>
                  </div>
                  <div class="ms-auto">
                        <div class="btn-group">
                              <button type="button" class="btn btn-primary">Settings</button>
                              <button type="button" class="btn btn-primary split-bg-primary dropdown-toggle dropdown-toggle-split" data-bs-toggle="dropdown">	<span class="visually-hidden">Toggle Dropdown</span>
                              </button>
                              <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg-end">	<a class="dropdown-item" href="javascript:;">Action</a>
                                    <a class="dropdown-item" href="javascript:;">Another action</a>
                                    <a class="dropdown-item" href="javascript:;">Something else here</a>
                                    <div class="dropdown-divider"></div>	<a class="dropdown-item" href="javascript:;">Separated link</a>
                              </div>
                        </div>
                  </div>
            </div>
            <!--end breadcrumb-->
            <div class="container">
                  <div class="main-body">
                        <div class="row">
                              <div class="col-lg-12">
                                    <form action="{{ route('active.vendor.approve') }}" method="post" enctype="multipart/form-data">
                                          @csrf
                                          <input type="hidden" name="id" value="{{ $inactiveVendor->id }}">
                                          <div class="card">
                                                <div class="card-body">
                                                      <div class="row mb-3">
                                                            <div class="col-sm-3">
                                                                  <h6 class="mb-0">Shop Name</h6>
                                                            </div>
                                                            <div class="col-sm-9 text-secondary">
                                                                  <input type="text" name="name" class="form-control" disabled value="{{ $inactiveVendor->name }}">
                                                            </div>
                                                      </div>
                                                      <div class="row mb-3">
                                                            <div class="col-sm-3">
                                                                  <h6 class="mb-0">Vendor Name</h6>
                                                            </div>
                                                            <div class="col-sm-9 text-secondary">
                                                                  <input type="text" name="username" class="form-control" value="{{ $inactiveVendor->username }}">
                                                            </div>
                                                      </div>
                                                      <div class="row mb-3">
                                                            <div class="col-sm-3">
                                                                  <h6 class="mb-0">Email</h6>
                                                            </div>
                                                            <div class="col-sm-9 text-secondary">
                                                                  <input type="text" name="email" class="form-control" value="{{ $inactiveVendor->email }}">
                                                            </div>
                                                      </div>
                                                      <div class="row mb-3">
                                                            <div class="col-sm-3">
                                                                  <h6 class="mb-0">Phone</h6>
                                                            </div>
                                                            <div class="col-sm-9 text-secondary">
                                                                  <input type="text" name="phone" class="form-control" value="{{ $inactiveVendor->phone }}">
                                                            </div>
                                                      </div>
                                                
                                                      <div class="row mb-3">
                                                            <div class="col-sm-3">
                                                                  <h6 class="mb-0">Address</h6>
                                                            </div>
                                                            <div class="col-sm-9 text-secondary">
                                                                  <input type="text" name="address" class="form-control" value="{{ $inactiveVendor->address }}">
                                                            </div>
                                                      </div>

                                                      <div class="row mb-3">
                                                            <div class="col-sm-3">
                                                                  <h6 class="mb-0">Join Date</h6>
                                                            </div>
                                                            <div class="col-sm-9 text-secondary">
                                                                  <input type="text" name="vendor_join" class="form-control" value="{{ $inactiveVendor->vendor_join }}">
                                                            </div>
                                                      </div>

                                                      <div class="row mb-3">
                                                            <div class="col-sm-3">
                                                                  <h6 class="mb-0">Vendor Info</h6>
                                                            </div>
                                                            <div class="col-sm-9 text-secondary">
                                                                  <input type="text" name="vendor_short_info" class="form-control" value="{{ $inactiveVendor->vendor_short_info }}">
                                                            </div>
                                                      </div>

                                                      <div class="row mb-3">
                                                            <div class="col-sm-3">
                                                                  <h6 class="mb-0">Image</h6>
                                                            </div>
                                                            <div class="col-sm-9 text-secondary">
                                                                  <input type="file" class="form-control" name="image" id="image">
                                                            </div>
                                                      </div>
                                                      <div class="row mb-3">
                                                            <div class="col-sm-3">
                                                            
                                                            </div>
                                                            <div class="col-sm-9">
                                                            <img id="showImg" src="{{ (!empty($inactiveVendor->image))?url('upload/vendorimg/'.$inactiveVendor->image):url('upload/noimg.png') }}" 
                                                            alt="img" width="50px">
                                                            </div>
                                                      </div>

                                                      <div class="row">
                                                            <div class="col-sm-3"></div>
                                                            <div class="col-sm-9 text-secondary">
                                                                  <input type="submit" class="btn btn-danger px-4" value="Approve">
                                                                  <a href="{{ route('pending_vendor') }}" class="btn btn-secondary">Cancel</a>
                                                            </div>
                                                      </div>
                                                </div>
                                          </div>
                                    </form>
                              </div>
                        </div>
                  </div>
            </div>
      </div>
</div>

@endsection