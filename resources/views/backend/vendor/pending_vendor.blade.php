@extends('admin.master')
@section('content')
<div class="page-wrapper">
   <div class="page-content">
      <!--breadcrumb-->
      <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
         <div class="breadcrumb-title pe-3">Vendor</div>
         <div class="ps-3">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb mb-0 p-0">
                  <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                  </li>
                  <li class="breadcrumb-item active" aria-current="page">All Pending Vendor</li>
               </ol>
            </nav>
         </div>
      </div>
      <!--end breadcrumb-->
    
      <div class="card">
         <div class="card-body">
            <div class="table-responsive">
               <div id="example_wrapper" class="dataTables_wrapper dt-bootstrap5">
                 
                  <div class="row">
                     <div class="col-sm-12">
                        <table id="example" class="table table-striped table-bordered dataTable" style="width: 100%;" role="grid" aria-describedby="example_info">
                           <thead>
                              <tr role="row">
                                 <th>Sl</th>
                                 <th>Shop Name</th>
                                 <th>Vendor UserName</th>
                                 <th>Join Date</th>
                                 <th>Email</th>
                                 <th>Status</th>
                                 <th>Action</th>
                              </tr>
                           </thead>
                           <tbody>
                              @foreach ($pendingVendor as $key => $item)
                                    <tr role="row" class="odd">
                                          <td class="sorting_1">{{ $key+1 }}</td>
                                          <td>{{ $item->name }}</td>
                                          <td>{{ $item->username }}</td>
                                          <td>{{ $item->vendor_join }}</td>
                                          <td>{{ $item->email }}</td>
                                          <th class="btn btn-info btn-sm">{{ $item->status }}</th>
                                          <td>
                                             <a href="{{ route('pending.vendor.details',$item->id) }}" title="edit" class="btn btn-primary btn-sm">Details</a>
                                          </td>
                                    </tr>
                              @endforeach
                           </tbody>
                        </table>
                        {{ $pendingVendor->links('pagination_link') }}
                     </div>
                  </div>
                 
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection