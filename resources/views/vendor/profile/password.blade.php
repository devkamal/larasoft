@extends('vendor.master')
@section('content')

<div class="page-wrapper">
      <div class="page-content"> 
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                  <div class="breadcrumb-title pe-3">Vendor Password</div>
                  <div class="ps-3">
                        <nav aria-label="breadcrumb">
                              <ol class="breadcrumb mb-0 p-0">
                                    <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Vendor Password</li>
                              </ol>
                        </nav>
                  </div>
             
            </div>
            <!--end breadcrumb-->
            <div class="container">
                  <div class="main-body">
                        <div class="row">
                           
                              <div class="col-lg-12">
                                    <form action="{{ route('update_vendor_password') }}" method="post" enctype="multipart/form-data">
                                          @csrf

                                         

                                          <div class="card">
                                                <div class="card-body">
                                                      <div class="row mb-3">
                                                            <div class="col-sm-3">
                                                                  <h6 class="mb-0">Old Passwrod</h6>
                                                            </div>
                                                            <div class="col-sm-9 text-secondary">
                                                                  <input type="password" name="oldpassword" class="form-control" placeholder="Old Password">
                                                                  @error('oldpassword')
                                                                        <sapn style="color:red">{{ $message }}</span>
                                                                  @enderror
                                                            </div>
                                                      </div>
                                                      <div class="row mb-3">
                                                            <div class="col-sm-3">
                                                                  <h6 class="mb-0">New Password</h6>
                                                            </div>
                                                            <div class="col-sm-9 text-secondary">
                                                                  <input type="password" name="new_password" class="form-control" placeholder="New Password">
                                                                  @error('new_password')
                                                                        <sapn style="color:red">{{ $message }}</span>
                                                                  @enderror
                                                            </div>
                                                      </div>
                                                      <div class="row mb-3">
                                                            <div class="col-sm-3">
                                                                  <h6 class="mb-0">Confirm Password</h6>
                                                            </div>
                                                            <div class="col-sm-9 text-secondary">
                                                                  <input type="password" name="confirm_password" class="form-control" placeholder="Confirm Password">
                                                                  @error('confirm_password')
                                                                        <sapn style="color:red">{{ $message }}</span>
                                                                  @enderror
                                                            </div>
                                                      </div>

                                                      <div class="row">
                                                            <div class="col-sm-3"></div>
                                                            <div class="col-sm-9 text-secondary">
                                                                  <input type="submit" class="btn btn-primary px-4" value="Save Changes">
                                                            </div>
                                                      </div>
                                                </div>
                                          </div>
                                    </form>
                              </div>
                        </div>
                  </div>
            </div>
      </div>
</div>


@endsection