@extends('vendor.master')
@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.2/css/all.min.css" integrity="sha512-1sCRPdkRXhBV2PBLUdRb4tMg1w2YPf37qatUFeS7zlBy7jJI8Lf4VHwWfZZfpXtYSLy85pkm9GaYVYMfw5BC1A==" crossorigin="anonymous" referrerpolicy="no-referrer" />

<div class="page-wrapper">
   <div class="page-content">
      <!--breadcrumb-->
      <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
         <div class="breadcrumb-title pe-3">Product</div>
         <div class="ps-3">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb mb-0 p-0">
                  <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                  </li>
                  <li class="breadcrumb-item active" aria-current="page">All Product: <span class="rounded-pill badge bg-danger">{{count($allData)}}</span></li>
               </ol>
            </nav>
         </div>
      </div>
      <!--end breadcrumb-->

     <div class="d-flex justify-content-between">
            <h6 class="mb-0 text-uppercase">Product List</h6>
            <a href="{{ route('add_vendor_product') }}" class="mb-0 text-uppercase btn btn-primary btn-sm">Add Product</a>
     </div>
    
      <hr>
      <div class="card">
         <div class="card-body">
            <div class="table-responsive">
               <div id="example_wrapper" class="dataTables_wrapper dt-bootstrap5">

               <div id="flash-message"></div>

                  <div class="row">
                     <div class="col-sm-12">
                        <table id="example" class="table table-striped table-bordered dataTable" style="width: 100%;" role="grid" aria-describedby="example_info">
                           <thead>
                              <tr role="row">
                                 <th>Sl</th>
                                 <th>Image</th>
                                 <th>Name</th>
                                 <th>Price</th>
                                 <th>QTY</th>
                                 <th>Discount</th>
                                 <th>Status</th>
                                 <th>Action</th>
                              </tr>
                           </thead>
                           <tbody>
                              @foreach ($allData as $key => $item)
                                    <tr role="row" class="odd">
                                          <td class="sorting_1">{{ $key+1 }}</td>
                                          <td> 
                                             <img src="{{ asset($item->thumbnail) }}" style="width: 70px; height:40px;" >  
                                          </td>
                                          <td>{{ $item->name }}</td>
                                          <td>{{ $item->price }}</td>
                                          <td>{{ $item->qty }}</td>

                                          <td>
                                             @if ($item->discount_price == NULL)
                                             <span class="badge reounded-pill bg-info text-danger">No Discount</span>
                                             @else
                                             @php
                                                $amount = $item->price - $item->discount_price;
                                                $discount = ($amount/$item->price) * 100;
                                             @endphp 
                                             <span class="badge rounded-pill bd-danger text-success">{{ round($discount) }}%</span>
                                             @endif
                                          
                                          </td>

                                          <td>
                                             @if ( $item->status == 1)
                                                <span class="text-success">Active</span>
                                                @else
                                                <span class="text-danger">InActive</span>
                                             @endif
                                          </td>
                                          <td>
                                                <a href="{{ route('vendor_details_product',$item->id) }}" title="details" class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                <a href="{{ route('edit_vendor_product',$item->id) }}" title="edit" class="btn btn-primary btn-sm"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                <a href="{{ route('delete_vendor_product',$item->id) }}" id="delete" title="delete" class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i></a>
   @if($item->status == 1)
   <a onclick="StatusInactive({{$item->id}})" class="btn btn-primary btn-sm" title="Active"> <i class="fa-solid fa-thumbs-up"></i> </a>
   @else
   <a onclick="StatusActive({{$item->id}})" class="btn btn-primary btn-sm" title="Inactive"> <i class="fa-solid fa-thumbs-down"></i> </a>
   @endif
                                          </td>
                                    </tr>
                              @endforeach
                           </tbody>
                        </table>
                        {{ $allData->links('pagination_link') }}
                     </div>
                  </div>
                 
               </div>
            </div>
         </div>
      </div>
   </div>
</div>


<script>
  function StatusInactive(id){
   //alert(id);
   $.ajaxSetup({
      headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
   });
   $.ajax({
      type:"GET",
      dataType:"json",
      url:"/get/inactive/"+id,
      success:function(id){
         alert('Do you want InActive?');
         $('#flash-message').html(
            `<div class="alert alert-success" role="alert">
            Product Inactive successfully!.
            </div>
            `
         );
      }
   });

  }
</script>

<script>
   function StatusActive(id){
      //alert(id);
      $.ajax({
         type:"GET",
         dataType:"json",
         url:"/get-active/"+id,
         success:function(data){
            alert('Do you want Active?');
            $('#flash-message').html(
            `<div class="alert alert-danger" role="alert">
               Product Inactive successfully!.
            </div>
            `);
         }
      });
   }
</script>

@endsection