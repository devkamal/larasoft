 @php 
    $route = Route::current()->getName();
 @endphp
<style>
    .contactnumber{
        height: 5px;
    }
</style>
 <!-- preloader-start -->
 <div id="preloader">
    <div class="rasalina-spin-box"></div>
</div>
<!-- preloader-end -->

<!-- Scroll-top -->
<button class="scroll-top scroll-to-target" data-target="html">
    <i class="fas fa-angle-up"></i>
</button>
<!-- Scroll-top-end-->

<!-- header-area -->
<header>
    <div id="sticky-header" class="menu__area transparent-header">
        <div class="container custom-container">
            <div class="row">
                <div class="col-md-12 contactnumber">
                      <marquee><span style="color:red">under processing our website. |</span>WhatsApp:+8801868799280. Email:flkamal2016@gmail.com.</marquee>
                </div>
                <div class="col-12">
                    <div class="mobile__nav__toggler"><i class="fas fa-bars"></i></div>
                    <div class="menu__wrap">
                        <nav class="menu__nav">
                            <div class="logo">
                                <a href="{{ url('/') }}" class="logo__black"><img src="{{ asset('frontend/assets/img/logo/logop.png') }}" alt=""></a>
                                <a href="{{ url('/') }}" class="logo__white"><img src="{{ asset('frontend/assets/img/logo/logop.png') }}" alt=""></a>
                            </div>
                            <div class="navbar__wrap main__menu d-none d-xl-flex">
                                <ul class="navigation">
                                    <li class="{{ ($route == 'home')? 'active' : '' }}"><a href="{{ route('home') }}">Home</a></li>

                                    <li class="menu-item-has-children {{ ($route == 'about_page')? 'active' : '' }}"><a href="#">Our Service</a>
                                        <ul class="sub-menu">
                                            <li><a href="portfolio-details.html">POS Website</a></li>
                                            <li><a href="portfolio-details.html">SMS Website</a></li>
                                            <li><a href="portfolio-details.html">LMS Website</a></li>
                                            <li><a href="portfolio-details.html">HRMS Website</a></li>
                                            <li><a href="portfolio-details.html">Booking Website</a></li>
                                            <li><a href="portfolio-details.html">Newsportal website</a></li>
                                            <li><a href="portfolio-details.html">Realstate Website</a></li>
                                            <li><a href="portfolio-details.html">PharmacyMS Website</a></li>
                                            <li><a href="portfolio-details.html">Website Maintenance</a></li>
                                            <li><a href="portfolio-details.html">Multi-vendor eCommerce Website</a></li>
                                        </ul>
                                    </li>

                                    <li class="menu-item-has-children {{ ($route == 'home_portfolio')? 'active' : '' }}"><a href="{{ route('home_portfolio') }}">Portfolio</a></li>

                                    <li class="{{ ($route == 'about_page')? 'active' : '' }}"><a href="{{ route('about_page') }}">About</a></li>

                                    <li class="menu-item-has-children {{ ($route == 'home_blog')? 'active' : '' }}"><a href="{{ route('home_blog') }}">Our Blog</a></li>

                                    <li class="menu-item-has-children {{ ($route == 'contact_me')? 'active' : '' }}"><a href="{{ route('contact_me') }}">Contact-us</a></li>

                                </ul>
                            </div>
                            <div class="header__btn d-none d-md-block">
                                <a href="{{ route('contact_me') }}" class="btn">Contact Us</a>
                            </div>
                        </nav>
                    </div>
                    <!-- Mobile Menu  -->
                    <div class="mobile__menu">
                        <nav class="menu__box">
                            <div class="close__btn"><i class="fal fa-times"></i></div>
                            <div class="nav-logo">
                                <a href="{{ url('/') }}" class="logo__black"><img src="{{ asset('frontend/assets/img/logo/logop.png') }}" alt=""></a>
                                <a href="{{ url('/') }}" class="logo__white"><img src="{{ asset('frontend/assets/img/logo/logop.png') }}" alt=""></a>
                            </div>
                            <div class="menu__outer">
                                <!--Here Menu Will Come Automatically Via Javascript / Same Menu as in Header-->
                            </div>
                            <div class="social-links">
                                <ul class="clearfix">
                                    <li><a href="#"><span class="fab fa-twitter"></span></a></li>
                                    <li><a href="#"><span class="fab fa-facebook-square"></span></a></li>
                                    <li><a href="#"><span class="fab fa-pinterest-p"></span></a></li>
                                    <li><a href="#"><span class="fab fa-instagram"></span></a></li>
                                    <li><a href="#"><span class="fab fa-youtube"></span></a></li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                    <div class="menu__backdrop"></div>
                    <!-- End Mobile Menu -->
                </div>
            </div>
        </div>
    </div>
</header>
<!-- header-area-end -->




