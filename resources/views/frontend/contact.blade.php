@extends('frontend.master')
@section('content')
@section('title')
Contact | Larasoft
@endsection
<style>
   .breadcrumb__wrap__icon::before {
   position: absolute;
   content: "";
   height: 100%;
   width: 100%;
   background-color: #000000;
   left: 0;
   right: 0;
   top: 0;
   z-index: 1;
   opacity: .60;
   }

/*================================================
Contact Info Area CSS
=================================================*/
.contact-info-box {
  text-align: center;
  border-radius: 5px;
  -webkit-transition: 0.5s;
  transition: 0.5s;
  -webkit-box-shadow: 0 2px 48px 0 rgba(0, 0, 0, 0.08);
          box-shadow: 0 2px 48px 0 rgba(0, 0, 0, 0.08);
  background: #ffffff;
  padding: 30px;
  margin-bottom: 30px;
  position: relative;
  z-index: 1;
}


.contact-info-box .icon {
  display: inline-block;
  width: 70px;
  height: 70px;
  line-height: 70px;
  background: #f5f5f5;
  border-radius: 50%;
  font-size: 35px;
  color: #f64c67;
  -webkit-transition: 0.5s;
  transition: 0.5s;
  margin-bottom: 12px;
  position: relative;
}

.contact-info-box .icon i {
  position: absolute;
  left: 0;
  top: 50%;
  -webkit-transform: translateY(-50%);
          transform: translateY(-50%);
  right: 0;
}

.contact-info-box h3 {
  margin-bottom: 12px;
  -webkit-transition: 0.5s;
  transition: 0.5s;
  font-size: 23px;
  font-weight: 700;
}

.contact-info-box p {
  margin-bottom: 2px;
  -webkit-transition: 0.5s;
  transition: 0.5s;
}

.contact-info-box p a {
  display: inline-block;
  color: #6b6b84;
}



</style>
<!-- main-area -->
<main>
   <!-- breadcrumb-area -->
   <section class="breadcrumb__wrap">
      <div class="container custom-container">
         <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-8 col-md-10">
               <div class="breadcrumb__wrap__content">
                  <h2 class="title text-white">Contact us</h2>
                  <nav aria-label="breadcrumb">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item text-white">Home /</li>
                        <li class="breadcrumb-item active" aria-current="page">Contact</li>
                     </ol>
                  </nav>
               </div>
            </div>
         </div>
      </div>
      <div class="breadcrumb__wrap__icon">
         <img src="https://www.webdigitalmantra.in/public/images/contact.png" alt="" width="100%" height="270px;">
      </div>
   </section>
   <!-- breadcrumb-area-end -->
   <div class="container">
      <div class="row">
         <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="contact-info-box">
               <div class="icon">
                  <img src="{{ asset('frontend/assets/img/icons/contact_icon03.png') }}" alt="">
               </div>
               <h3>Email Here</h3>
               <p><a href="mailto:flkamal2016@gmail.com">flkamal2016@gmail.com</a></p>
               <p><a href="mailto:hr@larasoft.info">hr@larasoft.info</a></p>
            </div>
         </div>
         <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="contact-info-box">
               <div class="icon">
                  <img src="{{ asset('frontend/assets/img/icons/contact_icon01.png') }}" alt="">
               </div>
               <h3>Connect Here</h3>
               <p class="pt-1">     </p>
               <ul class="social-links clearfix pl-0">
               
             
                  <a href="https://www.facebook.com/larasoft.info" target="_blank">
                  <img src="{{ asset('frontend/assets/img/facebook.png') }}" alt="Linkedin" style="text-decoration: none; width: 38px;">
                  </a>
                  <a target="_blank" href="https://www.linkedin.com/in/devkamalhossen/" alt="Facebook" style="text-decoration: none; width: 38px;">
                  <img src="{{ asset('frontend/assets/img/linkedin.png') }}" alt="Linkedin" style="text-decoration: none; width: 38px;">
                  </a>
                  <a target="_blank" href="https://twitter.com/KamalHo36371290">
                  <img target="_blank" src="{{ asset('frontend/assets/img/twitter.png') }}" alt="Twitter" style="text-decoration: none; width: 38px;">
                  </a>
                  <a target="_blank" href="https://www.instagram.com/flkamal2016/">
                  <img target="_blank" src="{{ asset('frontend/assets/img/instagram.webp') }}" alt="Instagram" style="text-decoration: none; width: 38px;">
                  </a>
               </ul>
               <p></p>
            </div>
         </div>
         <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="contact-info-box">
               <div class="icon">
                  <img src="{{ asset('frontend/assets/img/icons/contact_icon02.png') }}" alt="">
               </div>
               <h3>Call Here</h3>
               <p><a href="tel:+8801868799280">+8801868799280</a><br><a href="tel:+8801913556236">+8801913556236</a></p>
            </div>
         </div>
      </div>
   </div>
   <!-- contact-area -->
   <div class="contact-area">
      <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-center">Get in Touch</h3>
            </div>
        </div>
         <div class="row">
            <div class="col-md-6">
               <!-- contact-map -->
               <div id="contact-map" style="height:250px">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d96811.54759587669!2d-74.01263924803828!3d40.6880494567041!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c25bae694479a3%3A0xb9949385da52e69e!2sBarclays%20Center!5e0!3m2!1sen!2sbd!4v1636195194646!5m2!1sen!2sbd"
                     allowfullscreen loading="lazy"></iframe>
               </div>
               <!-- contact-map-end -->
            </div>
            <div class="col-md-6">
                  <form action="{{ route('store_message') }}" method="post" id="myForm">
                     @csrf
                     <div class="row">
                        <div class="col-md-6 mt-2">
                           <input type="text" name="name" placeholder="Enter your name*" class="form-control">
                           @error('name')
                           <span style="color:red">{{ $message }}</span>
                           @enderror
                        </div>
                        <div class="col-md-6 mt-2">
                           <input type="email" name="email" placeholder="Enter your mail*" class="form-control">
                           @error('email')
                           <span style="color:red">{{ $message }}</span>
                           @enderror
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-6 mt-2">
                           <input type="text" placeholder="Enter your subject*" name="subject" class="form-control">
                           @error('subject')
                           <span style="color:red">{{ $message }}</span>
                           @enderror
                        </div>
                        <div class="col-md-6 mt-2">
                           <input type="text" placeholder="Your phone*" name="phone" class="form-control">
                           @error('phone')
                           <span style="color:red">{{ $message }}</span>
                           @enderror
                        </div>
                     </div>
                 
                     <textarea name="message" id="message" placeholder="Enter your massage*" class="form-control mt-2"></textarea>
                     <button type="submit" class="btn mt-2">send massage</button>
           
                  </form>
            </div>
         </div>
      </div>
   </div>
   <!-- contact-area-end -->
   <!-- contact-area -->
   <section class="homeContact homeContact__style__two">
      <div class="container">
         <div class="homeContact__wrap">
            <div class="row">
               <div class="col-lg-6">
                  <div class="section__title">
                     <h2 class="title">Any questions? Feel free <br> to contact</h2>
                  </div>
              
               </div>
               <div class="col-lg-6">
                  <div class="homeContact__form">
                  <div class="homeContact__content">
                     <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form</p>
                     <h2 class="mail"><a href="mailto:flkamal2016@gmail.com">flkamal2016@gmail.com</a></h2>
                  </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!-- contact-area-end -->
</main>
<!-- main-area-end -->
@endsection