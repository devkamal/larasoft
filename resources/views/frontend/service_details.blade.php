@extends('frontend.master')
@section('content')
<style>
      .footer {
            padding: 30px 0 10px!important;
      }
</style>
        <!-- main-area -->
        <main>

            <!-- breadcrumb-area -->
            <section class="breadcrumb__wrap">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-xl-12 col-lg-12 col-md-12">
                            <div class="breadcrumb__wrap__content">
                                <h2 class="title">{{ $details->title }}</h2>
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">Details</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- breadcrumb-area-end -->

            <!-- services-details-area -->
            <section class="services__details">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="services__details__thumb">
                                <img src="{{ asset($details->image) }}" alt="img" width="100%">
                            </div>
                            <div class="services__details__content">
                                <h2 class="title">{{ $details->title }}</h2>
                                <p>{{ $details->description }}</p>
                               <ul class="services__details__list">
                                    @foreach ($lists as $item)
                                          <li>{{ $item->name }}</li>
                                    @endforeach
                                   
                                </ul>
                              </div>
                        </div>
                        <div class="col-lg-4">
                            <aside class="services__sidebar">
                                <div class="widget">
                                    <h5 class="title">Get in Touch</h5>
                                 
                                    <form action="{{ route('store_message') }}" method="post" id="myForm" class="sidebar__contact">
                                    @csrf
                                        <input type="text" placeholder="Enter name*" name="name">
                                        @error('name')
                                              <span class="text-danger">{{$message}}</span>
                                        @enderror
                                        <input type="email" name="email" placeholder="Enter your mail*">
                                        @error('email')
                                              <span class="text-danger">{{$message}}</span>
                                        @enderror

                                        <input type="number" placeholder="Enter your phone*" name="phone">
                                          @error('phone')
                                          <span style="color:red">{{ $message }}</span>
                                          @enderror

                                        <textarea name="message" id="message" placeholder="Massage*"></textarea>
                                        @error('message')
                                          <span style="color:red">{{ $message }}</span>
                                          @enderror

                                        <button type="submit" class="btn">send massage</button>
                                    </form>
                                </div>
                                <div class="widget">
                                    <h5 class="title">Contact Information</h5>
                                    <ul class="sidebar__contact__info">
                                        <li><span>Address :</span> 8638 Amarica Stranfod, <br> Mailbon Star</li>
                                        <li><span>Mail :</span> yourmail@gmail.com</li>
                                        <li><span>Phone :</span> +7464 0187 3535 645</li>
                                        <li><span>Fax id :</span> +9 659459 49594</li>
                                    </ul>
                                    <ul class="sidebar__contact__social">
                                        <li><a href="#"><i class="fab fa-dribbble"></i></a></li>
                                        <li><a href="#"><i class="fab fa-behance"></i></a></li>
                                        <li><a href="#"><i class="fab fa-linkedin"></i></a></li>
                                        <li><a href="#"><i class="fab fa-pinterest"></i></a></li>
                                        <li><a href="#"><i class="fab fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                                    </ul>
                                </div>
                            </aside>
                        </div>
                    </div>
                </div>
            </section>
            <!-- services-details-area-end -->


        </main>
        <!-- main-area-end -->

@endsection