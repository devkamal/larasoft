@extends('frontend.master')
@section('content')

@section('title')
    Home | Larasoft
@endsection

    @include('frontend.hero');

    <!-- services-area-start -->
    @include('frontend.singlepage.service')
    <!-- services-area-end -->

    <!-- portfolio-area -->
    @include('frontend.singlepage.portfolio')
    <!-- portfolio-area-end -->

    <!-- about-area -->
        @include('frontend.about')
    <!-- about-area-end -->

    <!-- blog-area -->
    @include('frontend.singlepage.blog')
    <!-- blog-area-end -->

    <!-- contact-area -->
    <section class="homeContact">
        <div class="container">
            <div class="homeContact__wrap">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="section__title">
                            <h2 class="title">Any questions? Feel free <br> to contact</h2>
                        </div>
                        <div class="homeContact__content">
                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form</p>
                            <h2 class="mail"><a href="mailto:hr@larasoft.info">hr@larasoft.info</a></h2>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="homeContact__form">
                        <form action="{{ route('store_message') }}" method="post" id="myForm">
                            @csrf
                        
                                <input type="text" name="name" id="name" placeholder="Enter name*">
                                @error('name')
                                    <span style="color:red">{{ $message }}</span>
                                @enderror

                                <input type="email" name="email" id="email" placeholder="Enter mail*">
                                @error('email')
                                    <span style="color:red">{{ $message }}</span>
                                @enderror

                                <input type="number" name="phone" id="phone" placeholder="Enter number*">
                                @error('phone')
                                    <span style="color:red">{{ $message }}</span>
                                @enderror

                                <textarea name="message" id="message" placeholder="Enter Massage*"></textarea>
                                @error('message')
                                    <span style="color:red">{{ $message }}</span>
                                @enderror

                                <button type="submit">Send Message</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- contact-area-end -->

</main>

@endsection