<!-- services-area -->
@php
      $services = App\Models\Service::all();
@endphp
<section class="services">
   <div class="container">
      <div class="services__title__wrap">
         <div class="row align-items-center justify-content-between">
            <div class="col-xl-5 col-lg-6 col-md-8">
               <div class="section__title">
                  <span class="sub-title">Our Services</span>
                  <h2 class="title">Creates amazing digital experiences</h2>
               </div>
            </div>
            <div class="col-xl-5 col-lg-6 col-md-4">
               <div class="services__arrow"></div>
            </div>
         </div>
      </div>
      <div class="row gx-0 services__active">
            @foreach ($services as $item)
                  <div class="col-xl-3">
                  <div class="services__item">
                  <div class="services__thumb">
                        <a href="{{ route('service_details',$item->id) }}"><img src="{{ asset($item->image) }}" alt=""></a>
                  </div>
                  <div class="services__content">
                        <div class="services__icon">
                        <img class="light" src="assets/img/icons/services_light_icon01.png" alt="">
                        <img class="dark" src="assets/img/icons/services_icon01.png" alt="">
                        </div>
                        <h3 class="title"><a href="{{ route('service_details',$item->id) }}">{{ $item->title }}</a></h3>
                        <p>{!! Str::limit($item->description, 60,'...') !!}</p>
                        @php
                           $servicelist = App\Models\ServiceList::where('service_id',$item->id)->limit(3)->get();
                        @endphp
                        <ul class="services__list">
                           @foreach ($servicelist as $list)
                              <li>{{ $list->name }}</li>
                           @endforeach
                         
                        </ul>
                        <a href="{{ route('service_details',$item->id) }}" class="btn border-btn">Read more</a>
                  </div>
                  </div>
            </div>
            @endforeach
      </div>
   </div>
</section>
<!-- services-area-end -->