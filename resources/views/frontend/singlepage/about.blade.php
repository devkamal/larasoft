@extends('frontend.master')
@section('content')
@section('title')
About | Larasoft
@endsection
@php
$abouts = App\Models\About::first();
@endphp
<!-- main-area -->
<main>
   <!-- breadcrumb-area -->
   <section class="breadcrumb__wrap">
      <div class="container custom-container">
         <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-8 col-md-10">
               <div class="breadcrumb__wrap__content">
                  <h2 class="title">About Us</h2>
                  <nav aria-label="breadcrumb">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">About Us</li>
                     </ol>
                  </nav>
               </div>
            </div>
         </div>
      </div>
      <div class="breadcrumb__wrap__icon">
         <ul>
            <li><img src="{{ asset('frontend/assets/img/icons/breadcrumb_icon01.png') }}" alt=""></li>
            <li><img src="{{ asset('frontend/assets/img/icons/breadcrumb_icon02.png') }}" alt=""></li>
            <li><img src="{{ asset('frontend/assets/img/icons/breadcrumb_icon03.png') }}" alt=""></li>
            <li><img src="{{ asset('frontend/assets/img/icons/breadcrumb_icon04.png') }}" alt=""></li>
            <li><img src="{{ asset('frontend/assets/img/icons/breadcrumb_icon05.png') }}" alt=""></li>
            <li><img src="{{ asset('frontend/assets/img/icons/breadcrumb_icon06.png') }}" alt=""></li>
         </ul>
      </div>
   </section>
   <!-- breadcrumb-area-end -->
   <!-- about-area -->
   <section class="about about__style__two mb-4">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-lg-6">
               <div class="about__image">
                  <img src="{{(!empty($abouts->image))?url('upload/aboutimg/'.$abouts->image):url('public/upload/no-img.png')}}" 
                     height="100%" width="100%" alt="img">
               </div>
            </div>
            <div class="col-lg-6">
               <div class="about__content">
                  <div class="section__title">
                     <span class="sub-title">01 - About Us</span>
                     <h2 class="title">{{ $abouts->title }}</h2>
                  </div>
                  <div class="about__exp">
                     <div class="about__exp__icon">
                        <img src="{{ asset('frontend/assets/img/icons/about_icon.png')}}" alt="">
                     </div>
                     <div class="about__exp__content">
                        <p><span>{{ $abouts->awards }}</p>
                     </div>
                  </div>
                  <p class="desc">{!! $abouts->description !!}</p>
               </div>
            </div>
         </div>
      </div>
   </section>
   <br><br> <br>
   <!-- about-area-end -->
   <!-- contact-area -->
   <section class="homeContact mt-4">
      <div class="container">
         <div class="homeContact__wrap">
            <div class="row">
               <div class="col-lg-6">
                  <div class="section__title">
                     <h2 class="title">Any questions? Feel free <br> to contact</h2>
                  </div>
               </div>
               <div class="col-lg-6">
                  <div class="homeContact__content">
                     <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form</p>
                     <h2 class="mail"><a href="mailto:flkamal2016@gmail.com">flkamal2016@gmail.com</a></h2>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!-- contact-area-end -->
</main>
<!-- main-area-end -->
@endsection