<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\BlogCategory;

class Blog extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function category(){
        return $this->belongsTo(BlogCategory::class, 'blog_category_id','id');
    }

    public function vendor(){
        return $this->belongsTo(User::class,'vendor_id','id');
    }
    
}
