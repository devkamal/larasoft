<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Home extends Model
{
    use HasFactory;

    protected $guarded = [];

    // protected $fillable = [
    //     'title',
    //     'sub_title',
    //     'image',
    //     'video',
    // ];

}
