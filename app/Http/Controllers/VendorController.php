<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules\Password;

class VendorController extends Controller
{
    public function VendorDashboard(){
       return view('vendor.index');
    }

    public function vendorLogout(){
        Auth::guard('web')->logout();
        return redirect()->route('vendor_login');
    }

    public function vendorLogin(){
        return view('vendor.login');
    }

    public function vendorProfile(){
        $id = Auth::user()->id;
        $vendorData = User::find($id);
        return view('vendor.profile.index',compact('vendorData'));
    }

    public function vendorUpdate(Request $request){
        $id   = Auth::user()->id;
        $data = User::find($id);
        $data->username = $request->username;
        $data->phone    = $request->phone;
        $data->email    = $request->email;
        $data->address  = $request->address;
        $data->vendor_join        = $request->vendor_join;
        $data->vendor_short_info  = $request->vendor_short_info;
        if($request->file('image')){
            $file = $request->file('image');
            @unlink(public_path('upload/vendorimg/'.$data->image));
            $filename = date('YdmHi').$file->getClientOriginalName();
            $file->move(public_path('upload/vendorimg'),$filename);
            $data['image'] = $filename;
        }
        $data->save();
        
        $notification = array(
            'alert-type' => 'Vendor profile updated success',
            'message' => 'success',
        );
        return redirect()->back()->with($notification);
    }

    public function vendorPassword(){
        return view('vendor.profile.password');
    }

    public function upVendorPass(Request $request){
        $validateData = $request->validate([
            'oldpassword' => 'required',
            'new_password' => 'required',
            'confirm_password' => 'required|same:new_password'
        ]);

        $hashPassword = Auth::user()->password;
        if(Hash::check($request->oldpassword,$hashPassword)){
            $users = User::find(Auth::id());
            $users->password = bcrypt($request->new_password);
            $users->save();

            session()->flash('message','Password change successfully!');
            return redirect()->route('vendor_login');
        }else{
            session()->flash('message','Oops something went wrong!');
            return redirect()->back();
        }
    }

    public function vendorRegister(){
        return view('auth.vendor_register');
    }

    public function vendorRegisterStore(Request $request)
    {
        $request->validate([
            'name'        => ['required', 'string', 'max:255'],
            'username'    => ['required'],
            'phone'       => ['required'],
            'vendor_join' => ['required'],
            'email'       => ['required', 'string', 'email', 'max:255', 'unique:'.User::class],
            'password'    => ['required', 'confirmed', Password::defaults()],
        ]);

        $user = User::insert([
            'name'     => $request->name,
            'username' => $request->username,
            'phone'    => $request->phone,
            'vendor_join' => $request->vendor_join,
            'email'    => $request->email,
            'password' => Hash::make($request->password),
            'role'     => 'vendor',
            'status'   => 'inactive',
        ]);

        $notification = array(
            'alert-type' => 'Vendor Register successfully.',
            'message' => 'success',
        );
        return redirect()->route('vendor_login')->with($notification);
    }
}
