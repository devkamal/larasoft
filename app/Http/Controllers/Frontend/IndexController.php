<?php

namespace App\Http\Controllers\Frontend;

use App\Models\User;
use App\Models\Product;
use App\Models\Category;
use App\Models\Multiimage;
use App\Models\SubCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{


    public function index(){
 
        return view('frontend.index');
    }

    public function VendorDetails($id){
        $vendorDetails = User::findOrFail($id);
        $vproducts = Product::where('vendor_id',$id)->get();
      
        return view('vendor.vendorlist.all_vendor',compact('vendorDetails','vproducts'));
    }

    public function VendorList(){
        $allVendor = User::where('status',1)->where('role','vendor')->orderBy('id','desc')->get();
        return view('vendor.vendorlist.all_vendor_list',compact('allVendor'));
    }

    public function CategoryWiseProduct($id,$slug){
        $product = Product::where('category_id',$id)->where('status',1)->orderBy('id','desc')->get();
        $categories = Category::orderBy('id','asc')->get();
        $breadcramb = Category::where('id',$id)->first();
        $newProduct = Product::orderBy('id','desc')->limit(3)->get();

       return view('frontend.product.category_view',compact('categories','product','breadcramb','newProduct'));
    }

    public function SubCategoryWiseProduct(Request $request, $id, $subcategory_slug){
        $product = Product::where('subcategory_id',$id)->where('status',1)->orderBy('id','desc')->get();
        $categories = Category::orderBy('id','asc')->get();
        $breadcramb = SubCategory::where('id',$id)->first();
        $newProduct = Product::orderBy('id','desc')->limit(3)->get();

       return view('frontend.product.subcategory_view',compact('categories','product','breadcramb','newProduct'));
    }

    // Produc view modal wise by ajax ************
    public function proModalView($id){
        $products = Product::with('category','brand')->findOrFail($id);
        $color = $products->color;
        $pro_color = explode(',', $color);

        $size = $products->size;
        $pro_size = explode(',', $size);

        return response()->json(array(
            'product' => $products,
            'size' => $pro_size,
            'color' => $pro_color,
        ));
    }

}
