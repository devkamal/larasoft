<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    public function AdminDashboard(){
        return view('admin.index');
    }

    public function AdminProfile(){
        $id = Auth::user()->id;
        $adminData = User::find($id);
        return view('admin.profile.index',compact('adminData'));
    }

    public function AdminStore(Request $request){
        $id = Auth::user()->id;
        $data = User::find($id);
        $data->username = $request->username;
        $data->phone = $request->phone;
        $data->email = $request->email;
        $data->address = $request->address;
        if($request->file('image')){
            $file = $request->file('image');
            @unlink(public_path('upload/adminimg/'.$data->image));
            $filename = date('YmdHi').$file->getClientOriginalName();
            $file->move(public_path('upload/adminimg'),$filename);
            $data['image'] = $filename;
        }
        $data->save();

        $notification = array(
            'message' => 'Admin updated successfully!!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }

    public function AdminPassword(){
        return view('admin.profile.password');
    }

    public function updatePassword(Request $request){
        $validateData = $request->validate([
            'oldpassword' => 'required',
            'new_password' => 'required',
            'confirm_password' => 'required|same:new_password'
        ]);

        $hashPassword = Auth::user()->password;
        if(Hash::check($request->oldpassword,$hashPassword)){
            $users = User::find(Auth::id());
            $users->password = bcrypt($request->new_password);
            $users->save();

            session()->flash('message','Password change successfully!');
            return redirect('logout');
        }else{
            session()->flash('message','Oops something went wrong!');
            return redirect()->back();
        }

    }

    //Login 
    public function AdminLogin(){
        return view('admin.login');
    }

    //Logout 
    public function AdminDestroy(Request $request)
    {
        Auth::guard('web')->logout();
        return redirect()->route('admin.login');
    }

    //Start of Vendor Pending & Approve logic with method
    public function vendorPending(){
        $pendingVendor = User::where('status','inactive')->where('role','vendor')->paginate('5');
        return view('backend.vendor.pending_vendor',compact('pendingVendor'));
    }

    public function vendorPendingDetails($id){
        $inactiveVendor = User::find($id);
        return view('backend.vendor.pending_details',compact('inactiveVendor'));
    }

    public function activeVendorApprove(Request $request){
        $vendor_id = $request->id;
        $user = User::find($vendor_id)->update([
            'status' => 'active',
        ]);

        $notification = array(
            'message' => 'Vendor Approve successfully!!',
            'alert-type' => 'success'
        );
        return redirect()->route('approve_vendor')->with($notification);
    }

    public function vendorApprove(){
        $approveVendor = User::where('status','active')->where('role','vendor')->paginate('5');
        return view('backend.vendor.approve_vendor',compact('approveVendor'));
    }

    public function vendorApproveDetails($id){
        $activeVendor = User::find($id);
        return view('backend.vendor.approve_details',compact('activeVendor'));
    }

    public function activeVendorCancel(Request $request){
        $vendor_id = $request->id;
        $user = User::find($vendor_id)->update([
            'status' => 'inactive',
        ]);

        $notification = array(
            'message' => 'Vendor Cancel successfully!!',
            'alert-type' => 'success'
        );
        return redirect()->route('pending_vendor')->with($notification);
    }
     //End of Vendor Pending & Approve logic with method

}
