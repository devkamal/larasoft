<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\About;
use App\Models\MultiImg;
use Image;
use DB;

class AboutController extends Controller
{
    public function index(){
        $data = About::find(1);
        return view('backend.about.index',compact('data'));
    }

    public function update(Request $request, $id){
        $validatedData = $request->validate([
            'title' => 'required',
            'awards' => 'required',
            'description' => 'required',
            'image' => 'required',
        ]);

        $data = About::find($id);
        $data->title = $request->title;
        $data->awards = $request->awards;
        $data->description = $request->description;
        
        /********* start of without intervention image  ***********/
        if($request->file('image')){
            $file = $request->file('image');
            @unlink(public_path('upload/aboutimg/'.$data->image));
            $filename = date('YmdHi').$file->getClientOriginalName();
            $file->move(public_path('upload/aboutimg/'),$filename);
            $data['image'] = $filename;
        }
        $data->update();
        return redirect()->back()->with('success','About updated successfully');
    }

    public function multiImg(){
        return view('backend.about.view_multiimg');
    }

    public function storeImg(Request $request){
        $multiimg = $request->file('multi_img');
        foreach($multiimg as $multi_img){
            $namegen = hexdec(uniqid()).'.'.$multi_img->getClientOriginalExtension();
            Image::make($multi_img)->resize(220,220)->save('upload/multiimg/'.$namegen);
            $saveurl = 'upload/multiimg/'.$namegen;

            MultiImg::insert([
                'multi_img' => $saveurl,
                'created_at' => Carbon::now()
            ]);
        }

        $notification = array(
            'message' => 'MultiImg added success',
            'alert-type' => 'success',
        );
        return redirect()->back()->with($notification);
    }

    public function allImg(){
        $allimgs = MultiImg::all();
        return view('backend.about.all_multiimg',compact('allimgs'));
    }

    public function editMultiImg($id){
        $editData = MultiImg::find($id);
        return view('backend.about.edit_multiimg',compact('editData'));
    }

    public function updateMultiImg(Request $request, $id){
        $data = array();
        $oldimg = $request->oldimg;
        $image = $request->multi_img;
        if($image){
            $image_one = uniqid().'.'.$image->getClientOriginalExtension();
            Image::make($image)->resize(220,220)->save('upload/multiimg/'.$image_one);
            $data['multi_img'] = 'upload/multiimg/'.$image_one;
            DB::table('multi_imgs')->where('id',$id)->update($data);
            unlink($oldimg);
           
            $notification = array(
                'message' => 'MultiImage updated success',
                'alert-type' => 'success'
            );
            return redirect()->route('all_img')->with($notification);
            
        }else{
            $data['multi_img'] = $oldimg;
            DB::table('multi_imgs')->where('id',$id)->update($data);
            $notification = array(
                'message' => 'Oops womething went wrong!',
                'alert-type' => 'error'
            );
            return redirect()->back()->with($notification);
        }
    }

    public function deleteImg($id){
        $allimgs = MultiImg::find($id);
        $oldimg = $allimgs->multi_img; 
        unlink($oldimg);

        MultiImg::find($id)->delete();

        $notification = array(
            'message' => 'Image deleted successfully!',
            'alert-type' => 'success',
        );
        return redirect()->back()->with($notification);
    }


    /*--------=========Start of Frontend area==========-----------*/
    public function aboutPage(){
        return view('frontend.singlepage.about');
    }









}
