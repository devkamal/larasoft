<?php

namespace App\Http\Controllers\Backend;

use Image;
use App\Models\Service;
use App\Models\ServiceList;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServiceController extends Controller
{
    public function view(){
        $allData = Service::all();
        return view('backend.service.view_service',compact('allData'));
    }

    public function add(){
        return view('backend.service.add_service');
    }

    public function store(Request $request){
        $validateData = $request->validate([
            'image' => 'required',
            'title' => 'required',
            'description' => 'required',
            'tags' => 'required',
        ]);

        $data = new Service();
        $data->tags = $request->tags;
        $data->title = $request->title;
        $data->description = $request->description;
        $image = $request->image;
        if($image){
            $img_one = uniqid().'.'.$image->getClientOriginalExtension();
            Image::make($image)->resize(320,240)->save('upload/service/'.$img_one);
            $data['image'] = 'upload/service/'.$img_one;
            $data->save();
        }

        $notification = array(
            'message' => 'Service added successfully',
            'alert-type' => 'success',
        );

        return redirect()->route('view_service')->with($notification);

    }

    public function edit($id){
        $editData = Service::findOrFail($id);
        return view('backend.service.edit_service',compact('editData'));
    }

    public function update(Request $request, $id){
        $oldimg = $request->oldimg;
        if($request->file('image')){
         $image = $request->file('image');
         $namegen = hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
         Image::make($image)->resize(320,240)->save('upload/service/'.$namegen);
         $save_url = 'upload/service/'.$namegen;
 
         Service::findOrFail($id)->update([
             'title' => $request->title,
             'tags' => $request->tags,
             'description' => $request->description,
             'image' => $save_url,
         ]);
 
         $notification = array(
             'message' => 'Service updated successfully',
             'alert-type' => 'success',
         );
 
         return redirect()->route('view_service')->with($notification);
 
        }else{
            Service::findOrFail($id)->update([
                 'title' => $request->title,
                 'tags' => $request->tags,
                 'description' => $request->description,
             ]);
 
             $notification = array(
                 'message' => 'Service updated without image',
                 'alert-type' => 'success',
             );
 
             return redirect()->route('view_service')->with($notification);
        }

    }

    public function delete($id){
        $deleteData = Service::find($id);
        $oldimg = $deleteData->image;
        unlink($oldimg);
        Service::find($id)->delete();

        $notification = array(
            'message' => 'Service remove successfully',
            'alert-type' => 'success',
        );

        return redirect()->back()->with($notification);
    }

    ///////////////*********ServiceList********** */
    public function viewlist(){
        $allData = ServiceList::all();
        $services = Service::all();
        return view('backend.service.view_service_list',compact('allData','services'));
    }

    public function storelist(Request $request){
        $validateData = $request->validate([
            'service_id' => 'required',
            'name' => 'required',
        ]);

        $data = new ServiceList();
        $data->service_id = $request->service_id;
        $data->name = $request->name;
        $data->save();

        $notification = array(
            'message' => 'ServiceList added successfully',
            'alert-type' => 'success',
        );

        return redirect()->route('view_service_list')->with($notification);

    }

    public function updatelist(Request $request, $id){
        $validateData = $request->validate([
            'service_id' => 'required',
            'name' => 'required',
        ]);

        $data = ServiceList::findOrFail($id);
        $data->service_id = $request->service_id;
        $data->name = $request->name;
        $data->update();

        $notification = array(
            'message' => 'ServiceList added successfully',
            'alert-type' => 'success',
        );

        return redirect()->route('view_service_list')->with($notification);

    }

    public function deletelist($id){
        ServiceList::find($id)->delete();
        $notification = array(
            'message' => 'ServiceList remove successfully',
            'alert-type' => 'success',
        );

        return redirect()->back()->with($notification);
    }

    ///////////////*************************Service Details */
    public function serviceDetails($id){
        $details = Service::findOrFail($id);
        $lists = ServiceList::where('service_id',$id)->orderBy('id','desc')->get();
        return view('frontend.service_details',compact('details','lists'));
    }

}
