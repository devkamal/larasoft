<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Contact;
use Illuminate\Support\Carbon;

class ContactController extends Controller
{
    public function contact(){
       return view('frontend.contact');
    }

    public function storeMsg(Request $request){
        $validateData = $request->validate([
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'message' => 'required',
        ]);

        Contact::insert([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'subject' => $request->subject,
            'message' => $request->message,
            'created_at' => Carbon::now(),
        ]);

        $notification = array(
            'message' => 'Thanks for contact-us!',
            'alert-type' => 'success',
        );
        return redirect()->back()->with($notification);

    }

    public function contactMsg(){
        $contacts = Contact::orderBy('id','desc')->get();
        return view('backend.contact.view_contact',compact('contacts'));
    }

    public function deleteMsg($id){
        Contact::find($id)->delete();
        $notification = array(
            'message' => 'Customer message deleted successfully!',
            'alert-type' => 'success',
        );
        return redirect()->back()->with($notification);
    }


}
