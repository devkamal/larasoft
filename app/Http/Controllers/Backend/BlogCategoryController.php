<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\BlogCategory;

class BlogCategoryController extends Controller
{
    public function viewCate(){
        $blogs = BlogCategory::all();
        return view('backend.blog_category.view_blog',compact('blogs'));
    }

    public function addCate(){
        return view('backend.blog_category.add_blog');
    }

    public function storeCate(Request $request){
        $validateData = $request->validate([
            'blog_category' => 'required',
        ]);

        $data = new BlogCategory();
        $data->blog_category = $request->blog_category;
        $data->save();

        $notification = array(
            'message' => 'Blog Category added successfully!',
            'alert-type' => 'success',
        );

        return redirect()->route('view_blog_category')->with($notification);

    }

    public function editCate($id){
        $editData = BlogCategory::find($id);
        return view('backend.blog_category.edit_blog',compact('editData'));
    }

    public function updateCate(Request $request, $id){
        $data = BlogCategory::find($id);
        $data->blog_category = $request->blog_category;
        $data->update();

        $notification = array(
            'message' => 'Blog Category updated successfully!',
            'alert-type' => 'success',
        );

        return redirect()->route('view_blog_category')->with($notification);
    }

    public function deleteCate($id){
        $deleteData = BlogCategory::find($id)->delete();

        $notification = array(
            'message' => 'Blog Category Deleted successfully!',
            'alert-type' => 'success',
        );

        return redirect()->back()->with($notification);
    }


}
