<?php

namespace App\Http\Controllers\Vendor;

use Image;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Brand;
use App\Models\Product;
use App\Models\Category;
use App\Models\Multiimage;
use App\Models\SubCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class VendorProductController extends Controller
{
    public function vendorAllProduct(){
        $id = Auth::user()->id;
        $allData = Product::where('vendor_id',$id)->orderBy('id','desc')->paginate(5);
        return view('vendor.product.all_product',compact('allData'));
    }

    public function addVendorProduct(){
        $brands = Brand::all();
        $categories = Category::all();
        return view('vendor.product.add_product',compact('brands','categories'));
    }

    public function VendorProductGetSubCategory($category_id){
        $subcat = SubCategory::where('category_id',$category_id)->orderBy('subcategory_name','ASC')->get();
        return response()->json($subcat);
    }

    public function StoreVendorProduct(Request $request){

        $image = $request->file('thumbnail');
        $name_gen = hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
        Image::make($image)->resize(800,800)->save('upload/productimg/thambnail/'.$name_gen);
        $save_url = 'upload/productimg/thambnail/'.$name_gen;
  
        $product_id = Product::insertGetId([
  
            'brand_id' => $request->brand_id,
            'category_id' => $request->category_id,
            'subcategory_id' => $request->subcategory_id,
            'name' => $request->name,
            'slug' => strtolower(str_replace(' ','-',$request->name)),
  
            'code' => $request->code,
            'qty' => $request->qty,
            'tags' => $request->tags,
            'size' => $request->size,
            'color' => $request->color,
  
            'price' => $request->price,
            'discount_price' => $request->discount_price,
            'short_description' => $request->short_description,
            'long_description' => $request->long_description, 
  
            'hot_deal' => $request->hot_deal,
            'feature' => $request->feature,
            'speacial_deal' => $request->speacial_deal,
            'speacial_offer' => $request->speacial_offer, 
  
            'thumbnail' => $save_url,
            'vendor_id' => Auth::user()->id,
            'status' => 1,
            'created_at' => Carbon::now(), 
  
        ]);
  
        /// Multiple Image Upload From her //////
  
        $images = $request->file('image');
        foreach($images as $img){
            $make_name = hexdec(uniqid()).'.'.$img->getClientOriginalExtension();
            Image::make($img)->resize(800,800)->save('upload/productimg/multi-image/'.$make_name);
            $uploadPath = 'upload/productimg/multi-image/'.$make_name;
  
            Multiimage::insert([
              'product_id' => $product_id,
              'image' => $uploadPath,
              'created_at' => Carbon::now(), 
           ]); 
        } // end foreach
  
        /// End Multiple Image Upload From her //////
  
        $notification = array(
            'message' => 'Product Inserted Successfully',
            'alert-type' => 'success'
        );
  
        return redirect()->route('vendor_allproduct')->with($notification); 
     }

     public function EditVendorProduct($id){
        $editData = Product::find($id);
        $brands = Brand::all();
        $categories = Category::all();
        $subcate = SubCategory::all();
        $multiimg = Multiimage::where('product_id',$id)->get();
    
        return view('vendor.product.edit_product',compact('editData','brands','categories','subcate','multiimg'));
     }

     public function UpdateVendorProduct(Request $request, $id){
  
        $product_id = Product::findOrFail($id)->update([
  
            'brand_id' => $request->brand_id,
            'category_id' => $request->category_id,
            'subcategory_id' => $request->subcategory_id,
            'name' => $request->name,
            'slug' => strtolower(str_replace(' ','-',$request->name)),
  
            'code' => $request->code,
            'qty' => $request->qty,
            'tags' => $request->tags,
            'size' => $request->size,
            'color' => $request->color,
  
            'price' => $request->price,
            'discount_price' => $request->discount_price,
            'short_description' => $request->short_description,
            'long_description' => $request->long_description, 
  
            'hot_deal' => $request->hot_deal,
            'feature' => $request->feature,
            'speacial_deal' => $request->speacial_deal,
            'speacial_offer' => $request->speacial_offer, 

            'vendor_id' => Auth::user()->id,
            'created_at' => Carbon::now(), 
            'status' => 1,
        ]);
  
  
        $notification = array(
            'message' => 'Product updated Successfully',
            'alert-type' => 'success'
        );
  
        return redirect()->route('vendor_allproduct')->with($notification); 
     }

     public function UpdateVendorMainImg(Request $request, $id){
        $oldimg = $request->oldimg;

        $image = $request->file('thumbnail');
        $name_gen = hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
        Image::make($image)->resize(800,800)->save('upload/productimg/thambnail/'.$name_gen);
        $save_url = 'upload/productimg/thambnail/'.$name_gen;

         if (file_exists($oldimg)) {
           unlink($oldimg);
        }

        Product::findOrFail($id)->update([
            'thumbnail' => $save_url,
            'updated_at' => Carbon::now(),
        ]);

       $notification = array(
            'message' => 'Vendor Product Main Image Updated Successfully',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
     }

     //Update MultiImage
     public function UpdateVendorMultiImg(Request $request, $id){
        
        $imgs = $request->image;

        foreach($imgs as $id => $img ){
            $imgDel = Multiimage::findOrFail($id);
            unlink($imgDel->image);

        $make_name = hexdec(uniqid()).'.'.$img->getClientOriginalExtension();
        Image::make($img)->resize(800,800)->save('upload/productimg/multi-image/'.$make_name);
        $uploadPath = 'upload/productimg/multi-image/'.$make_name;

        Multiimage::where('id',$id)->update([
            'image' => $uploadPath,
            'updated_at' => Carbon::now(),

        ]); 
        } // end foreach

         $notification = array(
            'message' => 'Vendor Product Multi Image Updated Successfully',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification); 
     }

     public function deleteVendorMultiImg($id){
        $oldImg = Multiimage::findOrFail($id);
        unlink($oldImg->image);

        Multiimage::findOrFail($id)->delete();

        $notification = array(
            'message' => 'Vendor Product Multi Image Deleted Successfully',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);

    }// End Method 


    public function DeleteVendorProduct($id){
        $product = Product::findOrFail($id);
        unlink($product->thumbnail);
        Product::findOrFail($id)->delete();

        $imges = Multiimage::where('product_id',$id)->get();
        foreach($imges as $img){
            unlink($img->image);
            Multiimage::where('product_id',$id)->delete();
        }

        $notification = array(
            'message' => 'Vendor Product Deleted Successfully',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }

    public function VendorProductDetails($id){
        $details = Product::findOrFail($id);
        $brands = Brand::all();
        $categories = Category::all();
        $subcate = SubCategory::all();
        $multiimg = Multiimage::where('product_id',$id)->get();
        $vendors = User::where('status','active')->where('role','vendor')->get();
        return view('vendor.product.vendor_details_product',compact('details','brands','categories','subcate','multiimg','vendors'));
    }


}
